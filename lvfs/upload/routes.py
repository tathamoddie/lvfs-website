#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-locals

import os
import hashlib
import json
from typing import Any

from flask import Blueprint, request, flash, url_for, redirect, render_template, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound

from lvfs import db, csrf, auth

from lvfs.util import (
    _get_client_address,
    _json_error,
    _json_success,
)
from lvfs.firmware.models import FirmwareRevision
from lvfs.vendors.models import Vendor, VendorAffiliation
from lvfs.tasks.models import Task
from lvfs.util import _get_sanitized_basename

from .utils import _upload_firmware_raw, _user_can_upload
from .uploadedfile import (
    FileNotSupported,
)

bp_upload = Blueprint("upload", __name__, template_folder="templates")


def _robot_upload_sync() -> Any:
    # uploading for a custom vendor
    if "vendor_id" in request.form:
        try:
            vendor = (
                db.session.query(Vendor)
                .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
                .one()
            )
        except NoResultFound:
            return _json_error("Specified vendor ID not found")
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        filename = request.files["file"].filename
        if not filename:
            return _json_error("Failed to upload: no filename")
        fn = f"{hashlib.sha256(blob).hexdigest()}-{_get_sanitized_basename(os.path.basename(filename))}"
    except KeyError as e:
        return _json_error(f"Failed to upload: {e!s}")
    if os.path.exists(fn):
        return _json_error(f"The same filename {fn} already exists")

    # continue with form data
    try:
        rev = FirmwareRevision(filename=fn)
        rev.write(blob)
        auto_delete: bool = False
        if "auto-delete" in request.form:
            auto_delete = True
        with db.session.no_autoflush:  # pylint: disable=no-member
            fw = _upload_firmware_raw(
                rev,
                task=Task(user=g.user),
                addr=_get_client_address(),
                vendor=vendor,
                target=request.form.get("target", "private"),
                auto_delete=auto_delete,
            )
            db.session.add(fw)
            db.session.commit()
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        return _json_error(str(e))

    # success
    return _json_success(
        msg="firmware created",
        uri=url_for("firmware.route_show", firmware_id=fw.firmware_id),
        errcode=201,
    )


@bp_upload.post("/token")
@auth.login_required
@csrf.exempt
def route_token() -> Any:
    """Upload a .cab file to the LVFS service with a username and token"""

    # just proxy
    return _robot_upload_sync()


@bp_upload.route("/", methods=["GET", "POST"])
@login_required
@csrf.exempt
def route_robot() -> Any:
    """Upload a .cab file to the LVFS service from a robot user"""

    # old URL being used
    if request.method != "POST":
        return redirect(url_for("upload.route_firmware"))

    # check is robot
    if not g.user.check_acl("@robot"):
        return _json_error("Not a robot user, please try again")

    # just proxy
    return _robot_upload_sync()


@bp_upload.route("/firmware", methods=["GET", "POST"])
@login_required
def route_firmware() -> Any:
    """Upload a .cab file to the LVFS service"""

    # only accept form data
    if request.method != "POST":
        if not hasattr(g, "user"):
            return redirect(url_for("main.route_index"))
        if not _user_can_upload(g.user):
            return redirect(url_for("agreements.route_show"))
        vendor_ids = [res.value for res in g.user.vendor.restrictions]
        affiliations = (
            db.session.query(VendorAffiliation)
            .filter(VendorAffiliation.vendor_id_odm == g.user.vendor_id)
            .all()
        )
        tasks = (
            db.session.query(Task)
            .filter(Task.user_id == g.user.user_id)
            .filter(Task.function == "lvfs.upload.utils.task_upload_firmware")
            .order_by(Task.task_id.desc())
            .limit(3)
            .all()
        )
        return render_template(
            "upload.html",
            category="firmware",
            vendor_ids=vendor_ids,
            affiliations=affiliations,
            tasks=tasks,
        )

    # uploading for a custom vendor
    if "vendor_id" in request.form:
        try:
            vendor = (
                db.session.query(Vendor)
                .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
                .one()
            )
        except NoResultFound:
            flash("Specified vendor ID not found", "warning")
            return redirect(url_for("upload.route_firmware"))
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        filename = request.files["file"].filename
        if not filename:
            flash("Failed to upload: no filename", "warning")
            return redirect(url_for("upload.route_firmware"))
        fn = f"{hashlib.sha256(blob).hexdigest()}-{_get_sanitized_basename(os.path.basename(filename))}"
    except KeyError as e:
        flash(f"Failed to upload: {e!s}", "warning")
        return redirect(url_for("upload.route_firmware"))
    if os.path.exists(fn):
        flash(f"The same filename {fn} already exists", "warning")
        return redirect(url_for("upload.route_firmware"))

    # continue with form data
    try:
        rev = FirmwareRevision(filename=fn)
        rev.write(blob, verify=True)
        db.session.add(rev)
        db.session.commit()
        auto_delete: bool = False
        if "auto-delete" in request.form:
            auto_delete = True
        fw = _upload_firmware_raw(
            rev,
            task=Task(user=g.user),
            sign=False,
            addr=_get_client_address(),
            vendor=vendor,
            target=request.form.get("target", "private"),
            auto_delete=auto_delete,
        )
        db.session.add(fw)
        db.session.commit()
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        flash(f"Failed to upload file: {e!s}", "warning")
        return redirect(url_for("upload.route_firmware"))

    # asynchronously sign
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=g.user,
            url=url_for("firmware.route_show", firmware_id=fw.firmware_id),
            function="lvfs.firmware.utils.task_sign_fw",
        )
    )
    db.session.commit()

    # success
    flash(f"Uploaded file {fw.revisions[0].filename}", "info")
    return redirect(url_for("firmware.route_show", firmware_id=fw.firmware_id))


@bp_upload.post("/firmware/async")
@login_required
def route_firmware_async() -> Any:
    """Upload a .cab file to the LVFS service"""

    # uploading for a custom vendor
    if "vendor_id" in request.form:
        try:
            vendor = (
                db.session.query(Vendor)
                .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
                .one()
            )
        except NoResultFound:
            flash("Specified vendor ID not found", "warning")
            return redirect(url_for("upload.route_firmware"))
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        filename = request.files["file"].filename
        if not filename:
            flash("Failed to upload: no filename", "warning")
            return redirect(url_for("upload.route_firmware"))
        fn = f"{hashlib.sha256(blob).hexdigest()}-{_get_sanitized_basename(os.path.basename(filename))}"
    except KeyError as e:
        flash(f"Failed to upload: {e!s}", "warning")
        return redirect(url_for("upload.route_firmware"))
    if os.path.exists(fn):
        flash(f"The same filename {fn} already exists", "warning")
        return redirect(url_for("upload.route_firmware"))

    # re-use existing upload revision, otherwise create
    rev = (
        db.session.query(FirmwareRevision)
        .filter(FirmwareRevision.filename == fn)
        .with_for_update(of=FirmwareRevision)
        .first()
    )
    if not rev:
        rev = FirmwareRevision(filename=fn)
        rev.write(blob)
        db.session.add(rev)
        db.session.commit()

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps(
                {
                    "id": rev.firmware_revision_id,
                    "filename": fn,
                    "addr": _get_client_address(),
                    "user_id": g.user.user_id,
                    "vendor_id": vendor.vendor_id,
                    "target": request.form.get("target", "private"),
                    "auto_delete": request.form.get("auto-delete", False),
                }
            ),
            caller=__name__,
            user=g.user,
            function="lvfs.upload.utils.task_upload_firmware",
        )
    )
    db.session.commit()

    flash(f"Starting upload for {rev.firmware_revision_id}", "info")
    return redirect(url_for("upload.route_firmware"))

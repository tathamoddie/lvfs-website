#!/usr/bin/python3
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_mirror(self, _app, client):
        self.login()
        self.upload()
        rv = client.get("/lvfs/mirrors/")
        assert "foobar" not in rv.data.decode("utf-8"), rv.data.decode()

        # create
        rv = client.post(
            "/lvfs/mirrors/create",
            data=dict(
                name="foobar",
            ),
            follow_redirects=True,
        )
        assert b"Added mirror" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/mirrors/")
        assert "lvfs" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/mirrors/create",
            data=dict(
                name="foobar",
            ),
            follow_redirects=True,
        )
        assert b"already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/mirrors/1/modify",
            data=dict(
                name="barbaz",
            ),
            follow_redirects=True,
        )
        assert b"Modified mirror" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/mirrors/")
        assert "barbaz" in rv.data.decode("utf-8"), rv.data.decode()

        # create a mirror_filter
        rv = client.post(
            "/lvfs/mirrors/1/mirror_filter/create",
            data={
                "key": "tag",
                "value": "hughski-2020q1",
            },
            follow_redirects=True,
        )
        assert b"Added filter" in rv.data, rv.data.decode()

        # show the filters page
        rv = client.get("/lvfs/mirrors/1/details")
        assert b"hughski-2020q1" in rv.data, rv.data.decode()

        # delete a mirror_filter
        rv = client.post(
            "/lvfs/mirrors/1/mirror_filter/1/delete", follow_redirects=True
        )
        assert b"Deleted filter" in rv.data, rv.data.decode()
        assert b"hughski-2020q1" not in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/mirrors/1/delete", follow_redirects=True)
        assert b"Deleted mirror" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/mirrors/")
        assert "barbaz" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()

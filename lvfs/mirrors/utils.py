#!/usr/bin/python3
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access,unused-argument,too-few-public-methods

from typing import Optional

import os
import sys
import hashlib
import lzma

import requests

from flask import current_app as app
from sqlalchemy.exc import NoResultFound, IntegrityError
from lxml import etree as ET

from lvfs import db
from lvfs.tasks.models import Task
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.firmware.utils import _firmware_delete
from lvfs.metadata.manifest import Manifest, ManifestItem
from lvfs.vendors.models import Vendor
from lvfs.protocols.models import Protocol
from lvfs.verfmts.models import Verfmt
from lvfs.categories.models import Category

from lvfs.upload.uploadedfile import FileNotSupported
from lvfs.upload.utils import _upload_firmware_raw
from lvfs.util import _get_client_address

from .models import Mirror, MirrorItem


class MirrorHelper:
    def __init__(self) -> None:
        self.id_vendors: dict[str, str] = {}


def _task_download_file(
    task: Task,
    mirror: Mirror,
    session: requests.Session,
    headers: dict[str, str],
    manifest_item: ManifestItem,
) -> Optional[MirrorItem]:
    fn = os.path.join(app.config["MIRROR_DIR"], mirror.name, manifest_item.filename)
    uri = os.path.join(mirror.uri, manifest_item.filename)

    # check if already exists with the correct checksum [using the cache]
    mirror_item = (
        db.session.query(MirrorItem)
        .filter(MirrorItem.mirror == mirror)
        .filter(MirrorItem.basename == manifest_item.filename)
        .first()
    )
    if mirror_item and mirror_item.checksum == manifest_item.checksum:
        mirror_item.valid = True
        return mirror_item

    # check if it already exists (e.g. we're importing production into QA)
    fw: Optional[Firmware] = (
        db.session.query(Firmware)
        .filter(Firmware.checksum_upload_sha256 == manifest_item.checksum)
        .first()
    )
    if fw and not fw.is_deleted:
        return None

    # check in the file already exists with the correct checksum
    try:
        with open(fn, "rb") as f:
            if hashlib.sha256(f.read()).hexdigest() == manifest_item.checksum:
                mirror_item = MirrorItem(
                    basename=manifest_item.filename,
                    checksum=manifest_item.checksum,
                    size=manifest_item.size,
                    valid=True,
                )
                mirror.items.append(mirror_item)
                return mirror_item
    except FileNotFoundError:
        pass

    # download
    try:
        rv = session.get(uri, headers=headers, timeout=60, stream=True)
    except requests.exceptions.ReadTimeout:
        task.add_fail(mirror.name, f"Timeout from {uri}")
        return None

    # probably permissions error
    if rv.status_code != 200:
        task.add_fail(mirror.name, f"Status code for {uri}: {rv.status_code}")
        return None
    checksum: str = hashlib.sha256(rv.content).hexdigest()
    if checksum != manifest_item.checksum:
        task.add_fail(
            mirror.name,
            f"Failed to download {uri}: got {checksum}, "
            f"expected {manifest_item.checksum}",
        )
        return None

    # success
    with open(fn, "wb") as f:
        f.write(rv.content)
    if mirror_item:
        mirror_item.size = manifest_item.size
        mirror_item.checksum = checksum
        mirror_item.valid = True
    else:
        mirror_item = MirrorItem(
            basename=manifest_item.filename,
            checksum=manifest_item.checksum,
            size=manifest_item.size,
            valid=True,
        )
        mirror.items.append(mirror_item)
    task.add_pass(mirror.name, f"Downloaded {uri}")
    return mirror_item


def _task_filter_mirror(mirror: Mirror, component: ET.Element) -> bool:
    # does the specified tag exist?
    for mirror_filter in mirror.filters:
        if mirror_filter.key == "tag":
            if mirror_filter.value not in [
                tag.text for tag in component.xpath("tags/tag")
            ]:
                return False

    # success
    return True


def _task_download_mirror(task: Task, mirror: Mirror) -> None:
    mirror_dir: str = os.path.join(app.config["MIRROR_DIR"], mirror.name)
    os.makedirs(mirror_dir, exist_ok=True)

    # use a different endpoint for embargo remotes
    uri: str = os.path.join(mirror.uri, "PULP_MANIFEST")
    if mirror.auth_username or mirror.auth_token:
        uri += "/vendor"

    session = requests.Session()
    headers: dict[str, str] = {
        "User-Agent": os.path.basename(sys.argv[0]),
    }
    if mirror.auth_token:
        headers["Authorization"] = mirror.auth_token

    # allow getting status
    mirror.size_total = 0
    mirror.size_now = 0
    for mirror_item in mirror.items:
        mirror_item.valid = False
        mirror.size_now += mirror_item.size
    mirror.task = task
    task.status = "Downloading PULP_MANIFEST"
    db.session.commit()

    try:
        rv = session.get(uri, headers=headers, timeout=60, stream=True)
    except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
        task.add_fail(mirror.name, f"Timeout from {uri}")
        return

    # probably permissions error
    if rv.status_code != 200:
        task.add_fail(mirror.name, f"Status code for {uri}: {rv.status_code}")
        return
    task.add_pass(mirror.name, f"Downloaded {uri}")

    # work out what to download
    task.status = "Parsing PULP_MANIFEST"
    db.session.commit()

    # download metadata
    verfmts: list[str] = []
    protocols: list[str] = []
    categories: list[str] = []
    to_download: list[ManifestItem] = []
    manifest = Manifest(rv.text)
    try:
        manifest_item_xml = manifest.get_by_filename_fnmatch("firmware*.xml.xz")[0]
    except ValueError:
        to_download.extend(manifest.get_by_filename_fnmatch("*.cab"))
    else:
        mirror_item = _task_download_file(
            task, mirror, session, headers, manifest_item_xml
        )
        if mirror_item:
            mirror.metadata_basename = mirror_item.basename
            mirror.size_total += mirror_item.size

            # open the .xml file to both get the cabinet basename, and also for filtering
            with lzma.open(mirror_item.absolute_path, mode="rb") as f:
                xml_parser = ET.XMLParser()
                components = ET.fromstring(f.read(), xml_parser).xpath(
                    "/components/component"
                )
                for component in components:
                    # matches filter
                    if not _task_filter_mirror(mirror, component):
                        continue

                    # add automatically
                    try:
                        verfmt: str = component.xpath(
                            "custom/value[@key='LVFS::VersionFormat']"
                        )[0].text
                        if verfmt not in verfmts:
                            verfmts.append(verfmt)
                    except IndexError:
                        pass
                    try:
                        protocol: str = component.xpath(
                            "custom/value[@key='LVFS::UpdateProtocol']"
                        )[0].text
                        if protocol not in protocols:
                            protocols.append(protocol)
                    except IndexError:
                        pass
                    try:
                        for category in component.xpath("categories/category"):
                            if category.text not in categories:
                                categories.append(category.text)
                    except IndexError:
                        pass

                    # cabinet archive
                    try:
                        location = component.xpath(
                            "releases/release/artifacts/artifact[@type='binary']/location"
                        )[0]
                        to_download.append(
                            manifest.get_by_filename_fnmatch(
                                os.path.basename(location.text)
                            )[0]
                        )
                    except IndexError:
                        pass
        else:
            mirror.metadata_basename = None
            to_download.extend(manifest.get_by_filename_fnmatch("*.cab"))
    for manifest_item in to_download:
        mirror.size_total += manifest_item.size
    task.percentage_total = len(to_download)
    db.session.commit()

    for value in protocols:
        try:
            db.session.add(Protocol(value=value, is_public=True))
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        else:
            task.add_pass(mirror.name, f"Added protocol {value}")
    for value in verfmts:
        try:
            db.session.add(Verfmt(value=value))
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        else:
            task.add_pass(mirror.name, f"Added version format {value}")
    for value in categories:
        try:
            db.session.add(Category(value=value))
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        else:
            task.add_pass(mirror.name, f"Added category {value}")

    # download each one
    for manifest_item in to_download:
        if task.ended_ts:
            break
        _task_download_file(task, mirror, session, headers, manifest_item)
        task.status = f"Checking {manifest_item.filename}"
        task.percentage_current += 1
        db.session.commit()

    # update stored size
    mirror.size_now = 0
    for mirror_item in mirror.items:
        mirror.size_now += mirror_item.size
    db.session.commit()


def _task_import_mirror_item(
    task: Task, mirror_item: MirrorItem, helper: MirrorHelper
) -> None:
    fw: Optional[Firmware] = (
        db.session.query(Firmware)
        .filter(Firmware.checksum_upload_sha256 == mirror_item.checksum)
        .first()
    )
    if fw:
        # correct the vendor name for pre-2024 firmware
        for md in fw.mds:
            if not md.developer_name:
                md.developer_name = helper.id_vendors.get(md.appstream_id)
                task.add_pass(
                    mirror_item.mirror.name,
                    f"Fixed #{md.appstream_id} with vendor {md.developer_name}",
                )

        # maybe make sure this is in the correct remote
        if fw.remote != mirror_item.mirror.remote:
            mirror_item.mirror.remote.is_valid = False
            fw.remote = mirror_item.mirror.remote
            fw.invalidate_metadata()
        return
    try:
        vendor: Vendor = (
            db.session.query(Vendor).filter(Vendor.group_id == "admin").one()
        )
    except NoResultFound:
        task.add_fail(mirror_item.mirror.name, "No admin vendor")
    task.status = f"Importing {mirror_item.basename}"
    db.session.commit()
    try:
        rev = FirmwareRevision(filename=mirror_item.basename)
        rev.write(mirror_item.blob)
        with db.session.no_autoflush:  # pylint: disable=no-member
            fw = _upload_firmware_raw(
                rev,
                task=task,
                addr=_get_client_address(),
                vendor=vendor,
                target=mirror_item.mirror.remote.name,
                auto_delete=False,
            )
            for md in fw.mds:
                if not md.developer_name:
                    md.developer_name = helper.id_vendors.get(md.appstream_id)
            db.session.add(fw)
            fw.invalidate_metadata()
            db.session.commit()
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        task.add_fail(mirror_item.mirror.name, str(e))


def _task_import_mirror(task: Task, mirror: Mirror, helper: MirrorHelper) -> None:
    # invalid
    if not mirror.remote:
        return

    # import new files
    mirror.task = task

    # use metadata to get developer_name
    if mirror.metadata_basename:
        task.status = f"Loading metadata from {mirror.metadata_basename}"
        db.session.commit()
        xml_parser = ET.XMLParser()
        metadata_blob = mirror.metadata_blob
        if metadata_blob:
            components = ET.fromstring(
                lzma.decompress(metadata_blob), xml_parser
            ).xpath("/components/component")
            for component in components:
                helper.id_vendors[component.xpath("id")[0].text] = component.xpath(
                    "developer_name"
                )[0].text

    # import all the mirror items
    task.status = "Importing mirror"
    task.percentage_current = 0
    task.percentage_total = len(mirror.items)
    db.session.commit()
    for mirror_item in mirror.items:
        if task.ended_ts:
            break
        if not mirror_item.valid:
            continue
        if mirror_item.basename.endswith(".cab"):
            _task_import_mirror_item(task, mirror_item, helper)
        task.percentage_current += 1


def _task_cleanup_mirror_item(
    task: Task, mirror_item: MirrorItem, helper: MirrorHelper
) -> None:
    # invalid
    if mirror_item.valid:
        return

    # delete file
    try:
        os.remove(mirror_item.absolute_path)
    except FileNotFoundError:
        pass
    task.add_pass(mirror_item.mirror.name, f"Deleted {mirror_item.basename}")

    # delete firmware
    fw: Optional[Firmware] = (
        db.session.query(Firmware)
        .filter(Firmware.checksum_upload_sha256 == mirror_item.checksum)
        .first()
    )
    if fw:
        _firmware_delete(fw, task)
        task.add_pass(
            mirror_item.mirror.name,
            f"Deleting #{fw.firmware_id} as {mirror_item.basename} disappeared",
        )

    # delete cache item
    db.session.delete(mirror_item)


def _task_cleanup_mirror(task: Task, mirror: Mirror, helper: MirrorHelper) -> None:
    # delete invalid items
    if not mirror.remove_deleted:
        return

    # update status
    task.status = f"Cleaning mirror {mirror.name}"
    task.percentage_current = 0
    task.percentage_total = len(mirror.items)
    db.session.commit()

    for mirror_item in mirror.items:
        if task.ended_ts:
            break
        _task_cleanup_mirror_item(task, mirror_item, helper)
        task.percentage_current += 1
        db.session.commit()


def task_download(task: Task) -> None:
    helper: MirrorHelper = MirrorHelper()
    for mirror in (
        db.session.query(Mirror)
        .filter(Mirror.enabled)
        .filter(Mirror.uri != None)
        .with_for_update(of=[Mirror])
    ):
        _task_download_mirror(task, mirror)
        if task.ended_ts:
            break
        _task_import_mirror(task, mirror, helper)
        if task.ended_ts:
            break
        _task_cleanup_mirror(task, mirror, helper)
        if task.ended_ts:
            break
        db.session.commit()

    # regenerate the remotes as required
    db.session.commit()

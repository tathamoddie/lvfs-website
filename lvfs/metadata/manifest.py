#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import os
import fnmatch
import hashlib

from typing import Optional

from flask import current_app as app

from lvfs.firmware.models import Firmware


class ManifestItem:
    def __init__(self, filename: str, checksum: str, size: int) -> None:
        self.filename: str = filename
        self.checksum: str = checksum
        self.size: int = size


class Manifest:
    def __init__(self, buf: Optional[str] = None) -> None:
        self._items_fn: dict[str, ManifestItem] = {}
        if buf:
            self.parse(buf)

    @property
    def items(self) -> list[ManifestItem]:
        return list(self._items_fn.values())

    def parse(self, buf: str) -> None:
        for line in buf.split("\n"):
            try:
                filename, checksum, size = line.rsplit(",", 2)
            except ValueError:
                continue
            self._items_fn[filename] = ManifestItem(filename, checksum, int(size))

    def get_by_filename_fnmatch(self, pattern: str) -> list[ManifestItem]:
        items: list[ManifestItem] = []
        for item in self.items:
            if fnmatch.fnmatch(item.filename, pattern):
                items.append(item)
        return items

    def add_file(
        self, filename: str, checksum: Optional[str] = None, size: Optional[int] = None
    ) -> None:
        if not checksum:
            with open(filename, "rb") as f:
                checksum = hashlib.sha256(f.read()).hexdigest()
        if not size:
            size = os.path.getsize(filename)
        basename = os.path.basename(filename)
        self._items_fn[basename] = ManifestItem(basename, checksum, size)

    def add_firmware(self, fw: Firmware) -> None:
        # add archive
        self.add_file(
            filename=fw.revisions[0].filename,
            checksum=fw.revisions[0].checksum_sha256,
            size=fw.mds[0].release_download_size,
        )

        # add update images
        for md in fw.mds:
            for url in [md.screenshot_url_safe, md.release_image_safe]:
                if not url:
                    continue
                fn = os.path.join(app.config["DOWNLOAD_DIR"], os.path.basename(url))
                if os.path.isfile(fn):
                    self.add_file(fn)

    def export(self) -> str:
        return (
            "\n".join(
                [f"{item.filename},{item.checksum},{item.size}" for item in self.items]
            )
            + "\n"
        )

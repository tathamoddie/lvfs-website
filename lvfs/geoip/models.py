#!/usr/bin/python3
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

from sqlalchemy import Column, Integer, Text, Numeric, Index

from lvfs import db


class Geoip(db.Model):  # type: ignore
    __tablename__ = "geoips"
    __table_args__ = (Index("idx_addr_start_addr_end", "addr_start", "addr_end"),)

    geoip_id = Column(Integer, primary_key=True)
    addr_start = Column(Numeric, nullable=False)
    addr_end = Column(Numeric, nullable=False)
    country_code = Column(Text, default=None)

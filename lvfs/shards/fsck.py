#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,unused-argument

from typing import List, Optional
import struct
import hashlib

from lvfs import db
from lvfs.tasks.models import Task
from lvfs.components.models import (
    ComponentShard,
    ComponentShardInfo,
    ComponentShardChecksum,
)


def _hash_like_acm(modulus: bytes, exponent: int = 0x10001) -> bytes:
    return modulus + struct.pack("<i", exponent)


def _fsck_shards_fixup_name(self: ComponentShard, task: Task) -> None:
    if not self.name:
        return
    self.name = self.name.replace("com.intel.Uefi", "org.uefi")


def _fsck_shard_infos_fixup_name(self: ComponentShardInfo, task: Task) -> None:
    if not self.name:
        return
    self.name = self.name.replace("com.intel.Uefi", "org.uefi")


def _fsck_shards_fixup_checksum(self: ComponentShard, task: Task) -> None:
    if self.guid not in [
        "aeec30fb-bd0c-55db-97e4-cfe52dc4fc90",  # com.intel.BootGuard.BootPolicy
        "a58290d6-ec14-5242-ab77-16006dc7d255",  # com.intel.BootGuard.KeyManifest
    ]:
        return

    # sanity check
    if not self.blob:
        return

    # include the exponent in the hash
    checksum_old = self.checksum
    checksum_new = hashlib.sha256(_hash_like_acm(self.blob)).hexdigest()
    if checksum_old != checksum_new:
        for csum in self.checksums:
            db.session.delete(csum)
        self.checksums.append(ComponentShardChecksum(value=checksum_new, kind="SHA256"))
        self.checksums.append(
            ComponentShardChecksum(
                value=hashlib.sha384(_hash_like_acm(self.blob)).hexdigest(),
                kind="SHA384",
            )
        )
        task.add_fail(
            "Shard::Checksum",
            f"Fixed up checksum from {checksum_old} to {checksum_new}",
        )


def _fsck(self: ComponentShard, task: Task, kinds: Optional[List[str]] = None) -> None:
    if not kinds or "checksum" in kinds:
        _fsck_shards_fixup_checksum(self, task)
    if not kinds or "name" in kinds:
        _fsck_shards_fixup_name(self, task)


def _fsck_info(
    self: ComponentShardInfo, task: Task, kinds: Optional[List[str]] = None
) -> None:
    if not kinds or "name" in kinds:
        _fsck_shard_infos_fixup_name(self, task)

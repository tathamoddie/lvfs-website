#!/usr/bin/python3
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,disable=singleton-comparison

from typing import Optional
import os
import fnmatch
import json

from sqlalchemy.exc import NoResultFound
from flask import g, url_for

from lvfs.tasks.models import Task
from lvfs.categories.models import Category
from lvfs import db

from .models import Component, ComponentDescription, ComponentGuid, ComponentRequirement


def _fsck_component_keywords(self: Component, task: Task) -> None:
    for kw in self.keywords:
        if kw.value == "fccl":
            task.add_fail(
                "Component::Keywords",
                "Repaired keyword {kw.value} firmware #{self.fw.firmware_id}",
            )
            kw.value = "fujitsu"


def _is_valid_update_message(val: Optional[str]) -> bool:
    if not val:
        return True

    for search in [
        "candidate BIOS",
        "Already fixed",
        "Enhanced the security of the system",
        "Enhancement to address security",
        "Finish Updating ELAN Fingerprint Reader",
        "AMD System Firmware Version",
        "Firmware version",
        "Problem fixes",
        "update for nrf_desktop",
        "contains the Dell system BIOS update",
        "Update includes",
    ]:
        if val.find(search) != -1:
            return False

    # fine for now, but we want to make this enumerated
    return True


def _fsck_component_update_message(self: Component, task: Task) -> None:
    if not _is_valid_update_message(self.release_message):
        task.add_fail(
            "Component::UpdateMessage",
            "Removed invalid message {self.release_message} #{self.fw.firmware_id}",
        )
        self.release_message = None


def _fsck_component_descriptions(self: ComponentDescription, task: Task) -> None:
    if not self.value:
        return

    # split this by line and by sentance looking for lines that should not exist
    new_lines = []
    for line in self.value.split("\n"):
        new_sens = []
        line = line.replace(" : ", ": ")
        line = line.replace("  ", " ")
        for sen in line.split("."):
            sen = sen.strip()
            for ele in [
                "FIX: ",
                "[IMPORTANT] ",
                "[Lenovo] ",
                "(Note) ",
            ]:
                sen = sen.replace(ele, "")
            sen_for_search = sen.lower()
            sen_for_search = sen_for_search.replace("warning: ", "")
            found = False
            for ele in [
                "do not turn off your computer or remove the ac adapt?r while *update *in progress",
                "do not turn off your computer while update is in progress",
                "the computer *be restarted *after updating * completely",
                "supported product scope*: lenovo *",
                "important updates* nothing",
                "new functions or enhancements* nothing",
            ]:
                if fnmatch.fnmatch(sen_for_search, ele):
                    found = True
                    break
            if not found:
                new_sens.append(sen)
        new_line = ".".join(new_sens)
        for ele in [
            "The computer will be restarted automatically after updating EC completely.",
            "The computer will be restarted automatically after updating ECFW completely.",
            "The computer will be restarted automatically after updating BIOS completely.",
            "The computer will be restarted automatically after updating MEFW completely .",
            "The computer will be restarted automatically after updating BIOS completely .",
            "The computer will be restarted automatically after updating firmware completely .",
            "The computer will be restarted automatically after updating completely.",
            "The computer will be restarted automatically after updating completely .",
            "The device may not properly function until you shut down or reboot PC.",
            "The device may not properly function until you shut down or reboot PC",
            "The computer shall be restarted after updating firmware completely. ",
            "* The computer shall be restarted after updating firmware completely",
            "Do NOT turn off your computer or remove the AC adapter while update is in progress ",
        ]:
            new_line = new_line.replace(ele, "")
        new_lines.append(new_line.replace("  ", " ").strip())

    new_tx_value = "\n".join(new_lines)
    while new_tx_value.find("\n\n\n") != -1:
        new_tx_value = new_tx_value.replace("\n\n\n", "\n\n")
    new_tx_value = new_tx_value.strip()
    if self.value != new_tx_value:
        task.add_fail(
            "Component::ReleaseDescription",
            f"Fixed description {self.value} -> {new_tx_value} #{self.component_id}",
        )
        self.value = new_tx_value


def _fsck_component_translations(self: Component, task: Task) -> None:
    # both map to actual None for the default...
    for tx in self.descriptions:
        if tx.locale in ["None", "en_US"]:
            tx.locale = None
            task.add_fail(
                "Database::Translations",
                "Repaired translation locale from firmware #{self.fw.firmware_id}",
            )

    # look for dups
    locales: dict[str, bool] = {}
    to_remove: list[ComponentDescription] = []
    for tx in self.descriptions:
        if tx.locale in locales:
            to_remove.append(tx)
            continue
        locales[tx.locale] = True

    # actually remove the dups
    for tx in to_remove:
        task.add_fail(
            "Database::Translations",
            f"Removing duplicate {tx.locale} translation locale from firmware #{self.fw.firmware_id}",
        )
        self.descriptions.remove(tx)


def _fsck_component_version_format(self: Component, task: Task) -> None:
    # already set
    if self.verfmt:
        return

    # does there exist another firmware with the same GUID and the verfmt set?
    verfmts = {}
    for md in (
        db.session.query(Component)
        .filter(Component.verfmt_id != None)
        .join(ComponentGuid)
        .filter(ComponentGuid.value.in_(self.guid_values))
        .order_by(Component.release_timestamp.desc())
    ):
        verfmts[md.verfmt_id] = md

    # gahh, no data
    if not verfmts:
        task.add_fail(
            "Component::VersionFormat",
            f"Component #{self.component_id} has no discoverable version format",
        )
        return

    # even worse, different data for the same GUID
    if len(verfmts) > 1:
        component_ids: list[str] = []
        for md in verfmts.values():
            component_ids.append(f"#{md.component_id}")
        task.add_fail(
            "Component::VersionFormat",
            f"Components {','.join(component_ids)} have multiple version formats",
        )
        return

    # success
    self.verfmt_id = list(verfmts.keys())[0]
    task.add_pass(
        "Component::VersionFormat",
        f"Repaired component #{self.component_id} with VersionFormat {self.verfmt_id} ",
    )


def _fsck_component_requirement_empty(self: Component, task: Task) -> None:
    # find any empty requirements
    to_remove: list[ComponentRequirement] = []
    for req in self.requirements:
        if req.kind in ["client", "id", "hardware"]:
            if not req.value:
                task.add_pass(
                    "Component::requirement",
                    f"Removed empty {req.kind} requirement for #{self.component_id} ",
                )
                to_remove.append(req)
    for req in to_remove:
        self.requirements.remove(req)

    # this is important enough to resign the archive and metadata
    if to_remove:
        db.session.add(
            Task(
                value=json.dumps({"id": self.fw.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                function="lvfs.firmware.utils.task_sign_fw",
                force=True,
            )
        )
        db.session.commit()


def _fsck_component_requirement_duplicate(self: Component, task: Task) -> None:

    # find any empty requirements
    reqs_remove: list[ComponentRequirement] = []
    reqs_seen: dict[str, ComponentRequirement] = {}
    for req in self.requirements:
        if str(req) in reqs_seen:
            task.add_pass(
                "Component::requirement",
                f"Removed duplicate {req.kind} requirement for #{self.component_id} ",
            )
            reqs_remove.append(req)
            continue
        reqs_seen[str(req)] = req
    for req in reqs_remove:
        self.requirements.remove(req)


def _fsck_component_category(self: Component, task: Task) -> None:
    if self.category and self.category.value != "X-Device":
        return

    # convert icons to categories
    for icon, category_value in {
        "battery": "X-Battery",
    }.items():
        if self.icon == icon:
            try:
                self.category = (
                    db.session.query(Category)
                    .filter(Category.value == category_value)
                    .one()
                )
            except NoResultFound:
                task.add_fail(
                    "Component::category",
                    f"No category {category_value} ",
                )
                return
            task.add_pass(
                "Component::category",
                f"Converted component #{self.component_id} from icon {self.icon} to category {self.category.value}",
            )
            self.icon = None
            return

    for needle, category_value in {
        "Fingerprint Reader": "X-FingerprintReader",
        "MOH Reader": "X-FingerprintReader",
        "SSD": "X-SolidStateDrive",
        "NVMe": "X-SolidStateDrive",
        "NVM": "X-ThunderboltController",
        "USB-C Dock": "X-UsbDock",
        "Dock": "X-Dock",
        "Keyboard": "X-Keyboard",
        "Mouse": "X-Mouse",
    }.items():
        if self.name.find(needle) != -1:
            try:
                self.category = (
                    db.session.query(Category)
                    .filter(Category.value == category_value)
                    .one()
                )
                self.name = self.name.replace(needle, "").replace("  ", " ").strip()
            except NoResultFound:
                task.add_fail(
                    "Component::category",
                    f"No category {category_value} ",
                )
                return
            task.add_pass(
                "Component::category",
                f"Repaired component #{self.component_id} with category {self.category.value} ",
            )
            return

    for protocol_value, category_value in {
        "org.flashrom": "X-System",
        "org.nvmexpress": "X-SolidStateDrive",
    }.items():
        if self.protocol and self.protocol.value == protocol_value:
            try:
                self.category = (
                    db.session.query(Category)
                    .filter(Category.value == category_value)
                    .one()
                )
            except NoResultFound:
                task.add_fail(
                    "Component::category",
                    f"No category {category_value}",
                )
                return
            task.add_pass(
                "Component::category",
                f"Set component #{self.component_id} to category {self.category.value}",
            )
            return


def _fsck_component_download_size(self: Component, task: Task) -> None:

    try:
        download_size: int = os.path.getsize(self.fw.revisions[0].absolute_path)
    except FileNotFoundError:
        return
    if download_size != self.release_download_size:
        task.add_fail(
            "Component::category",
            f"Fxied Component #{self.component_id} size from "
            f"{self.release_download_size} to {download_size} bytes",
        )
        self.release_download_size = download_size


def _fsck(self: Component, task: Task, kinds: Optional[list[str]] = None) -> None:
    if not kinds or "download-size" in kinds:
        _fsck_component_download_size(self, task)
    if not kinds or "translations" in kinds:
        _fsck_component_translations(self, task)
    if not kinds or "keywords" in kinds:
        _fsck_component_keywords(self, task)
    if not kinds or "update-message" in kinds:
        _fsck_component_update_message(self, task)
    if not kinds or "version-format" in kinds:
        _fsck_component_version_format(self, task)
    if not kinds or "category" in kinds:
        _fsck_component_category(self, task)
    if not kinds or "requirement" in kinds:
        _fsck_component_requirement_empty(self, task)
        _fsck_component_requirement_duplicate(self, task)
    if not kinds or "update-descriptions" in kinds:
        for tx in self.descriptions:
            _fsck_component_descriptions(tx, task)

#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime

from sqlalchemy import Column, Integer, Text, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.associationproxy import association_proxy

from lvfs import db


class ProtocolDeviceFlag(db.Model):  # type: ignore
    __tablename__ = "protocol_device_flags"
    protocol_device_flag_id = Column(Integer, primary_key=True)
    protocol_id = Column(Integer, ForeignKey("protocol.protocol_id"))
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    value = Column(Text, nullable=False, unique=True)

    protocol = relationship("Protocol", back_populates="device_flags")
    user = relationship("User", foreign_keys=[user_id])

    def __repr__(self) -> str:
        return f"ProtocolDeviceFlag({self.protocol_device_flag_id}:{self.value})"


class Protocol(db.Model):  # type: ignore
    __tablename__ = "protocol"

    protocol_id = Column(Integer, primary_key=True)
    value = Column(Text, nullable=False, unique=True)
    name = Column(Text, default=None)
    icon = Column(Text, default=None)
    is_signed = Column(Boolean, default=False)
    is_transport = Column(Boolean, default=False)
    is_public = Column(Boolean, default=False)
    can_verify = Column(Boolean, default=False)
    has_header = Column(Boolean, default=False)
    require_report = Column(Boolean, default=False)
    verfmt_id = Column(Integer, ForeignKey("verfmts.verfmt_id"))
    allow_custom_update_message = Column(Boolean, default=False)
    signing_algorithm = Column(Text, default=None)
    update_request_id = Column(Text, default=None)
    update_message = Column(Text, default=None)
    update_image = Column(Text, default=None)
    comment = Column(Text, default=None)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)
    allow_device_category = Column(Boolean, default=False)

    verfmt = relationship("Verfmt", foreign_keys=[verfmt_id])
    device_flags = relationship(
        "ProtocolDeviceFlag",
        back_populates="protocol",
        cascade="all,delete,delete-orphan",
    )
    device_flag_values = association_proxy("device_flags", "value")

    def __repr__(self) -> str:
        return f"Protocol({self.protocol_id},{self.value})"

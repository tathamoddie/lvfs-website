#!/usr/bin/python3
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import os

from collections import defaultdict
from typing import Any

from flask import Blueprint, render_template, request, url_for, redirect, flash
from flask import current_app as app
from flask_login import login_required

from lvfs import db, ploader

from lvfs.firmware.models import Firmware
from lvfs.pluginloader import PluginBase
from lvfs.tests.models import Test, TestResult
from lvfs.util import _event_log, _get_settings, admin_login_required

from .models import Setting

bp_settings = Blueprint("settings", __name__, template_folder="templates")


def _convert_tests_for_plugin(plugin: PluginBase) -> dict[str, list[Test]]:
    tests_by_type: dict[str, list[Test]] = defaultdict(list)
    for test in (
        db.session.query(Test)
        .join(Firmware)
        .filter(Test.plugin_id == plugin.id)
        .order_by(Test.scheduled_ts.desc())
    ):
        if len(tests_by_type["recent"]) < 20:
            tests_by_type["recent"].append(test)
        if test.is_pending:
            tests_by_type["pending"].append(test)
        elif test.is_running:
            tests_by_type["running"].append(test)
        elif test.waived_ts:
            tests_by_type["waived"].append(test)
        elif test.result == TestResult.PASS:
            tests_by_type["success"].append(test)
        elif test.result == TestResult.WARN:
            tests_by_type["warning"].append(test)
        else:
            tests_by_type["failed"].append(test)
    return tests_by_type


@bp_settings.route("/")
@bp_settings.route("/<plugin_id>")
@login_required
@admin_login_required
def route_view(plugin_id: str = "general") -> Any:
    """
    Allows the admin to change details about the LVFS instance
    """
    plugin = ploader.get_by_id(plugin_id)
    if not plugin:
        flash(f"No plugin {plugin_id}", "danger")
        return redirect(url_for("settings.route_view"))
    tests_by_type = _convert_tests_for_plugin(plugin)
    return render_template(
        "settings.html",
        category="settings",
        settings=_get_settings(),
        plugin=plugin,
        tests_by_type=tests_by_type,
    )


@bp_settings.route("/<plugin_id>/tests/<kind>")
@login_required
@admin_login_required
def route_tests(plugin_id: str, kind: str) -> Any:
    """
    Allows the admin to change details about the LVFS instance
    """
    plugin = ploader.get_by_id(plugin_id)
    if not plugin:
        flash(f"No plugin {plugin_id}", "danger")
        return redirect(url_for("settings.route_view"))
    tests_by_type = _convert_tests_for_plugin(plugin)
    return render_template(
        "settings-tests.html",
        category="settings",
        kind=kind,
        tests=tests_by_type[kind][:50],
        tests_by_type=tests_by_type,
        plugin=plugin,
    )


@bp_settings.route("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # sanity check
    for dirname in ["DOWNLOAD_DIR", "SHARD_DIR", "UPLOAD_DIR"]:
        if not os.path.isdir(app.config[dirname]):
            flash(f"Directory {app.config[dirname]} does not exist", "warning")

    # create all the plugin default keys
    settings = _get_settings()
    for plugin in ploader.get_all():
        for s in plugin.settings:
            if s.key not in settings:
                db.session.add(Setting(key=s.key, value=s.default))
    db.session.commit()
    return redirect(url_for("settings.route_view"))


def _textarea_string_to_text(value_unsafe: str) -> str:
    values: list[str] = []
    for value in value_unsafe.replace("\r", "").split("\n"):
        value = value.strip()
        if value:
            values.append(value)
    return "\n".join(values)


@bp_settings.route("/modify", methods=["GET", "POST"])
@bp_settings.route("/modify/<plugin_id>", methods=["GET", "POST"])
@login_required
@admin_login_required
def route_modify(plugin_id: str = "general") -> Any:
    """Change details about the instance"""

    # only accept form data
    if request.method != "POST":
        return redirect(url_for("settings.route_view", plugin_id=plugin_id))

    # save new values
    settings = _get_settings()
    for key in request.form:
        if key == "csrf_token":
            continue
        if settings[key] == request.form[key]:
            continue
        setting = db.session.query(Setting).filter(Setting.key == key).first()
        setting.value = _textarea_string_to_text(request.form[key])
        _event_log(f"Changed server settings {key} to {setting.value}")
    db.session.commit()
    flash("Updated settings", "info")
    return redirect(url_for("settings.route_view", plugin_id=plugin_id), 302)

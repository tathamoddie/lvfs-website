#!/usr/bin/python3
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import importlib
import datetime

from sqlalchemy.exc import IntegrityError

from lvfs import db

from .models import Task, TaskScheduler


def _run_task(task: Task) -> None:
    try:
        (modulename, funcname) = task.function.rsplit(".", maxsplit=1)
        module = importlib.import_module(modulename)
        func = getattr(module, funcname)
        func(task)
        db.session.commit()
    except BaseException as e:  # pylint: disable=broad-except
        task.add_fail("Failed to run", repr(e))
        db.session.commit()


def task_run_all() -> None:
    for task in (
        db.session.query(Task)
        .filter(Task.started_ts == None)
        .with_for_update(of=Task)
        .order_by(Task.priority.desc())
    ):
        task.started_ts = datetime.datetime.utcnow()
        db.session.commit()
        _run_task(task)
        task.ended_ts = datetime.datetime.utcnow()
        db.session.commit()


def task_autodelete(task: Task) -> None:
    now = datetime.datetime.utcnow()
    deleted: list[str] = []
    for task_tmp in (
        db.session.query(Task)
        .filter(Task.created_ts < now - datetime.timedelta(days=14))
        .order_by(Task.created_ts.asc())
    ):
        try:
            db.session.delete(task_tmp)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            continue
        deleted.append(f"#{task_tmp.task_id}")
    if deleted:
        task.add_pass(f"autodeleted old tasks: {', '.join(deleted)}")

    # just check to see if we can make this a db constraint
    for task_tmp in db.session.query(Task).filter(Task.user is None):
        task.add_fail(f"Task {task_tmp.value} has no user")


def task_create_schedulers(task: Task) -> None:

    task_schedulers: list[TaskScheduler] = [
        TaskScheduler(
            function="lvfs.geoip.utils.task_import_url",
            interval=24 * 30,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.users.utils.task_email_report",
            interval=24 * 30,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.reports.utils.task_regenerate", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.main.utils.task_regenerate", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.firmware.utils.task_autodelete",
            interval=24,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.analytics.utils.task_generate", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.users.utils.task_disable", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.shards.utils.task_regenerate", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.tasks.utils.task_autodelete", interval=24, priority=-10
        ),
        TaskScheduler(
            function="lvfs.vendors.utils.task_update_attrs",
            interval=1,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.tests.utils.task_run_all", interval=1, priority=-10
        ),
        TaskScheduler(
            function="lvfs.main.utils.task_client_anonymize",
            interval=24,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.mirrors.utils.task_download",
            interval=24,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.metadata.utils.task_regenerate_dirty_remotes_embargo",
            interval=1,
            priority=-10,
        ),
        TaskScheduler(
            function="lvfs.metadata.utils.task_regenerate_dirty_remotes_public",
            interval=4,
            priority=-10,
        ),
    ]
    for task_scheduler in task_schedulers:
        try:
            db.session.add(task_scheduler)
            db.session.commit()
            task.add_pass(f"created scheduler: {task_scheduler.function}")
        except IntegrityError:
            db.session.rollback()

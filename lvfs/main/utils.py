#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import datetime

from lvfs import db

from lvfs.util import _get_datestr_from_datetime
from lvfs.components.models import ComponentShardInfo, ComponentShard, Component
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.protocols.models import Protocol
from lvfs.reports.models import Report
from lvfs.hsireports.models import HsiReport
from lvfs.tests.models import Test
from lvfs.users.models import User
from lvfs.vendors.models import Vendor
from lvfs.tasks.models import Task
from lvfs.clients.models import Client, ClientMetric


def task_regenerate(task: Task) -> None:
    values: dict[str, int] = {}
    values["ClientCnt"] = db.session.query(Client.id).count()
    values["FirmwareCnt"] = db.session.query(Firmware.firmware_id).count()
    values["FirmwareStableCnt"] = (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name == "stable")
        .count()
    )
    values["FirmwareTestingCnt"] = (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name == "testing")
        .count()
    )
    values["FirmwarePrivateCnt"] = (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.is_public == False)
        .count()
    )
    values["TestCnt"] = db.session.query(Test.test_id).count()
    values["ReportCnt"] = db.session.query(Report.report_id).count()
    values["HsiReportCnt"] = db.session.query(HsiReport.hsi_report_id).count()
    values["ProtocolCnt"] = db.session.query(Protocol.protocol_id).count()
    values["ComponentShardInfoCnt"] = db.session.query(
        ComponentShardInfo.component_shard_info_id
    ).count()
    values["ComponentShardCnt"] = db.session.query(
        ComponentShard.component_shard_id
    ).count()
    values["ComponentCnt"] = db.session.query(Component.component_id).count()
    values["VendorCnt"] = (
        db.session.query(Vendor.vendor_id)
        .filter(Vendor.visible)
        .filter(Vendor.username_glob != None)
        .count()
    )
    values["UserCnt"] = (
        db.session.query(User.user_id).filter(User.auth_type != "disabled").count()
    )

    #  save to database
    for key, value in values.items():
        metric = (
            db.session.query(ClientMetric)
            .filter(ClientMetric.key == key)
            .with_for_update(of=ClientMetric)
            .first()
        )
        if not metric:
            metric = ClientMetric(key=key)
            db.session.add(metric)
        metric.value = value
        task.add_pass(f"{metric.key}={metric.value}")
    db.session.commit()


def task_client_anonymize(task: Task) -> None:
    datestr: int = _get_datestr_from_datetime(
        datetime.datetime.today() - datetime.timedelta(days=365 * 3)
    )
    db.session.query(Client).filter(Client.datestr < datestr).update(
        {Client.user_agent: None, Client.country_code: None, Client.addr: None}
    )
    db.session.commit()

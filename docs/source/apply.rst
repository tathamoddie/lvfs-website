Getting an Account
##################

There is no charge to vendors for opening an account or for distribution of content.
You can start the process by `opening a ticket <https://gitlab.com/fwupd/lvfs-website/-/issues/new>`_
with as much information you have, or just with questions or for more details.

Information to Supply
=====================

* The vendor full legal name
* The public homepage for this vendor
* A link to a high resolution logo for the vendor
* The domain used for email address assigned to this vendor, e.g. ``@realtek.com,@realtek.com.tw``
* The update protocol are you using, and if it is already supported in fwupd
* Legal permission that you have the required permission to upload to the LVFS. There is an
  `example document <https://gitlab.com/fwupd/lvfs-website/-/raw/master/docs/upload-permission.doc>`_ which
  can be modified, signed, and uploaded as an attachment to the GitLab issue.
  We can create a vendor account *without this*, but the account will not be able to push firmware to
  the public remotes until this document is provided.
* The ``Vendor ID`` for all hardware uploaded by this vendor (from ``fwupdmgr get-devices`` e.g. ``USB:0x046D``)
* The reverse DNS AppStream ID namespace prefix you plan to use for all uploaded firmware, e.g. ``com.hp``
* The URL to use for any possible security incident response (PSIRT), e.g. ``https://www.vendor.com/security``
* An assigned "vendor manager" that can create new accounts on the LVFS in the future, and be the primary point of contact
* If you going to be acting as an ODM or IHV to another vendor, e.g. uploading firmware on their behalf

If you are acting as an ODM or IHV to another vendor:

* Which OEM(s) will you be uploading for?
* Do you have a contact person for the OEM? If so, who?
* Will you be QAing the update and pushing to stable yourselves, or letting the OEM do this?

.. note::
  If you wish for the ticket to remain private (only viewable by the LVFS administrators)
  you **must** mark it as confidential as otherwise the ticket is viewable by public users:

  .. figure:: img/confidental-issue.png
    :align: center
    :width: 342px

.. note::
  Vendors who can upload firmware updates are in a privileged position where files
  can be installed on end-user systems without authentication.
  This means we have to do careful checks on new requests, which may take a few
  days to complete.

Vendor Groups
=============

On the LVFS there are several classes of user that can be created.
By default users are created as *upload only* which means they can only view
firmware uploaded by themselves.

Users can be *promoted* to QA users by the vendor manager so that they can see
(and optionally modify) other firmware in their vendor group.
QA users are typically the people that push firmware to the testing and stable
remotes.

There can be multiple vendor groups for large OEMs, for instance an OEM might
want a *storage* vendor group that is isolated from the *BIOS* team.
Alternatively, vendors can use Azure to manage users on the LVFS.
Contact the LVFS administrator for more details if you would like to use this.

Adding Users
------------

The vendor manager can add users to an existing vendor group.
If the vendor manager has additional privileges (e.g. the permission to push to stable)
then these can also be set for the new user.

New users have to match the username domain glob, so if the value for the vendor
is ``@realtek.com,@realtek.com.tw`` then ``dave@realtek.com.tw`` could be added by
the vendor manager -- but ``dave@gmail.com`` would be forbidden.

Trusted Users
-------------

Vendor groups are created initially as ``untrusted`` which means no users can
promote firmware to testing and stable.

Once a valid firmware has been uploaded correctly and been approved by someone
in the the LVFS admin team we will *unlock* the user account to the ``trusted``
state which allows users to promote firmware to the public remotes.

.. note::
  In most cases we also need some kind of legal document that shows us that
  the firmware is legally allowed to be redistributed by the LVFS.

  For instance, something like this is usually required:

  *<vendor> is either the sole copyright owner of all uploaded firmware,
  or has permission from the relevant copyright owner(s) to upload files to
  Linux Vendor Firmware Service Project a Series of LF Projects, LLC (known as the “LVFS”)
  for distribution to end users.
  <vendor> gives the LVFS explicit permission to redistribute the
  unmodified firmware binary however required without additional restrictions,
  and permits the LVFS service to analyze the firmware package for any purpose.
  <signature>, <date>, <title>*

Export Control
==============

Some firmware may contain binary code that has been deemed subject to some kind
of export control.
The exact meaning of *export control* has been defined in various places,
including Export Administration Regulations (EAR), and International Traffic
in Arms Regulations (ITAR).

Code capable of strong encryption like AES, RSA or 3DES may be subject to
export control and it may be forbidden to distribute to users located in specific
embargoed countries like Cuba, Iran, North Korea, Sudan or Syria.

.. note::
  Although there is a specific and notable export exception for “software updates”
  in EAR, it should of course be the decision of the legal team of the OEM to
  make the decision themselves.

The list of countries is usually specified *per-vendor* which means it is applied
for all firmware in the vendor account.
It can also be specified *per-firmware*, which might be useful where just one
specific firmware is explicitly covered under export control, for instance
for a model only designed to be sold to the US military.
This can be specified in the metadata block for the firmware ``component``:

.. code-block:: xml

      <custom>
        <value key="LVFS::BannedCountryCodes">IR,SY</value>
      </custom>

Only LVFS admin team and vendor manager can edit the vendor export control list.
It is specified according to ISO3166, which would typically be ``CU,IR,KP,SD,SY``
for most large vendors.

.. note::
  Like all other services hosting files, the LVFS uses GeoIP data to identify
  which country the user is downloading files from.
  This is not a perfect science, and although the assigned list of IP blocks is
  updated daily some false positives and false negatives can occur.

End User License Agreements
===========================

Legal teams of vendors sometimes request that we make the end user agree to a
license agreement or legal declaration before deploying the update.
There are several reasons why have chosen to not support EULAs:

* The majority of updates applied in the enterprise are done “*unattended*” and
  also done at scale with thousands of devices.
  Forcing the end-user to do any interactive action makes these automated or
  “*headless*” updates impossible.
* Allowing other users to “*pre-accept*” the end-user license agreement isn’t
  what this legal mechanism was designed for – for example is it legally binding
  if the junior sysadmin accepts the agreement on the end-users behalf?
  Or does it have to be accepted by someone from the destination legal team with
  the authority to do so – which needs to be recorded for audit purposes.
* Vendors often want to use a “*generic*” boilerplate legal agreement that controls
  how the user is allowed to use the hardware using overly broad language that is
  either not applicable to the device, totally confusing to the end user, or by
  adding restrictions on an already purchased product.
* Vendors often want to show a EULA so that if broken firmware gets deployed then
  it becomes the users fault for attempting the upgrade action and the vendor
  cannot be considered responsible in any way.
  **This isn’t fair to customers** – risky or untested updates should never be
  pushed to millions of end users.
* LVFS is **used all over the world**, and users might not even understand the
  language the EULA is written in.
  Legal jurisdictions also differ between the nations of this world, and the EULA
  might not be legally binding or permissible.

We've been asked to add support for EULAs a few times and the answer has always been no.
The almost-universal consensus from the community was that allowing EULAs is a terrible
idea that would be a slippery slope, encouraging vendors to take the *easy option* and
show pages of overly restrictive boilerplate legalese for each update.

If your legal department disagrees, please let them know that every vendor shipping
firmware on the LVFS has agreed that a EULA was not actually required.

.. note::

  The UI can show the release notes and an optional update message, but it is purely
  advisory and the user is free to ignore or suppress it -- by disabling the
  condition in the source code or even patching the binary executable.
  The front-end client (e.g. GNOME Software or Google Chrome) also has no
  requirement to implement showing either.
  This UI was not designed for EULA text and should not be used in this way.

Alternate Branches
==================

We typically only allow the silicon vendor, the ODM or the OEM to upload firmware
for hardware, and only if that entity has legal permission to upload the file to
the LVFS.
The security model for fwupd relies on standardized registries like USB and PCI,
along with immutable DMI information to ensure that only the correct vendors
can ship firmware for their own hardware, and nothing else.

This strict rule breaks down where the OEM responsible for the hardware considers
the device *end-of-life* and so will no longer receive updates (even for
critical security issues).
There may also be a situation where there exists an alternate (not provided by
the vendor) free software re-implementation of the proprietary firmware, which
may be desired for licencing reasons.

In these situations we allow another legal entity to also upload firmware for the
hardware, but with a few restrictions:

* The user must manually and explicitly opt-in to the new firmware stream,
  perhaps using ``fwupdmgr switch-branch``, with a suitable warning that there
  is no vendor support available and that the hardware warranty is now invalid.
  This means that the alternate firmware must set the device branch
  appropriately without any additional configuration.
* The alternate firmware must not ship with any code, binaries or generated
  assets from the original hardware vendor (perhaps including trademarks) unless
  written permission is provided in writing by the appropriate vendor.

Some real world examples might be providing a Open Source BCM57xx GPL firmware
for Broadcom network hardware, or providing a coreboot system firmware for a
long-EOLed Lenovo X220 ThinkPad.
In this instance, the LVFS may be the legal entity distributing the firmware,
which is actually provided by a trusted contributor who has permissions to upload
and hardware to test the update.
In other cases another legal entity (like coreboot itself) or an individual
trusted contributor may be considered the distributor.

In **all** cases the specifics should be discussed with the LVFS maintainers,
as should any concerns by licensors or existing distributors.

.. note::

  It is insanity to throw a perfectly working machine into landfill just because
  it’s considered EOL by the original hardware vendor and no longer receiving
  security updates.

  If we can help provide alternate safe firmware, these machines then provide
  inexpensive access for education and employment for those otherwise unable to
  afford devices.

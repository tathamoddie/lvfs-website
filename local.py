#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import os
import sys
import argparse
from typing import List

from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.metadata.utils import _generate_metadata_kind
from lvfs.upload.uploadedfile import UploadedFile, MetadataInvalid


def parse_args():
    parser = argparse.ArgumentParser(
        description="Generate local metadata to use with fwupd"
    )
    parser.add_argument(
        "--archive-directory",
        default=".",
        help="Local directory of CAB archives to scan",
    )
    parser.add_argument(
        "--basename",
        default="firmware",
        help="Target system: relative directory to metadata location",
    )
    parser.add_argument(
        "--metadata", default="metadata.xml.gz", help="Full path to metadata"
    )
    args = parser.parse_args()
    return args


def create_metadata(archive_dir, basename, metadata_fn):
    # process all archives in directory
    fws: List[Firmware] = []
    print(f"Searching {archive_dir}")
    for root, dirs, files in os.walk(archive_dir):  # pylint: disable=unused-variable
        for filename in files:
            if not filename.endswith(".cab"):
                continue
            print(f"Processing {filename}...")
            ufile = UploadedFile()
            try:
                revision = FirmwareRevision(filename=filename)
                with open(os.path.join(root, filename), "rb") as f:
                    ufile.parse(revision, data=f.read())
                ufile.fw.revisions.append(revision)
            except MetadataInvalid as e:
                print(f"Failed to parse {filename}: {e}")
            else:
                fws.append(ufile.fw)

    # write metadata
    print(f"Writing {metadata_fn}")
    xml = _generate_metadata_kind(fws, firmware_baseuri=f"{basename}/", local=True)
    with open(metadata_fn, "wb") as f:
        f.write(xml)


if __name__ == "__main__":
    ARGS = parse_args()
    create_metadata(ARGS.archive_directory, ARGS.basename, ARGS.metadata)
    sys.exit(0)

# Custom template
"""empty message

Revision ID: 6b9c338048c9
Revises: 6a69eba1f05d
Create Date: 2023-05-09 21:37:31.091051

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "6b9c338048c9"
down_revision = "6a69eba1f05d"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("hsi_report_attrs", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_hsi_report_attrs_appstream_id"),
            ["appstream_id"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("hsi_report_attrs", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_hsi_report_attrs_appstream_id"))

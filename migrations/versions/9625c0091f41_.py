# Custom template
"""empty message

Revision ID: 9625c0091f41
Revises: a3cc33e92890
Create Date: 2023-04-08 16:42:14.527961

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "9625c0091f41"
down_revision = "a3cc33e92890"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "protocol_device_flags",
        sa.Column("protocol_device_flag_id", sa.Integer(), nullable=False),
        sa.Column("protocol_id", sa.Integer(), nullable=True),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.ForeignKeyConstraint(
            ["protocol_id"],
            ["protocol.protocol_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("protocol_device_flag_id"),
        sa.UniqueConstraint("value"),
    )
    with op.batch_alter_table("components", schema=None) as batch_op:
        batch_op.add_column(sa.Column("device_flags", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("components", schema=None) as batch_op:
        batch_op.drop_column("device_flags")
    op.drop_table("protocol_device_flags")

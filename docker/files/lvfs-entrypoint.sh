#!/bin/bash
set -e

function ecs_metadata {
    if [ -n "$ECS_CONTAINER_METADATA_URI_V4" ]; then
        curl ${ECS_CONTAINER_METADATA_URI_V4} --output /tmp/container.json
        export CONTAINER_ID=`jq -r -s '.[0].DockerId' /tmp/container.json`
    else
        export CONTAINER_ID="unset"
    fi
    echo "CONTAINER_ID is $CONTAINER_ID"
}

if [ "$DEPLOY" = "application" ]
then
    ecs_metadata
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade
    echo "starting gunicorn"
    export DD_EXPVAR_PORT="5555"
    export DD_ENV=`echo $LVFS_CONFIG | jq -r .release`
    export DD_SERVICE="LVFS"
    export DD_LOGS_INJECTION="true"
    export DD_PROFILING_ENABLED="true"
    export DD_APPSEC_ENABLED="true"
    export ECS_FARGATE="true"
    exec ddtrace-run gunicorn --config /app/conf/gunicorn.py "wsgi:app"
fi

if [ "$DEPLOY" = "metadata" ]
then
    ecs_metadata
    echo "starting freshclam"
    freshclam # download our files first
    freshclam -v --stdout -d -c 1 --daemon-notify=/etc/clamd.d/scan.conf # then daemonize
    echo "starting clam"
    /usr/sbin/clamd -c /etc/clamd.d/scan.conf
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade
    echo "starting worker"
    export DD_EXPVAR_PORT="5555"
    export DD_ENV=`echo $LVFS_CONFIG | jq -r .release`
    export DD_SERVICE="LVFS"
    export DD_LOGS_INJECTION="true"
    export DD_PROFILING_ENABLED="true"
    export DD_APPSEC_ENABLED="true"
    export ECS_FARGATE="true"
    PYTHONPATH=. exec ddtrace-run python lvfs/worker.py
fi

if [ "$DEPLOY" = "test" ]
then
    source /app/venv/bin/activate
    pip install mypy
fi

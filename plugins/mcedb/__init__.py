#!/usr/bin/python3
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import os
import sqlite3

from lvfs import db
from lvfs.pluginloader import PluginBase, PluginSetting, PluginSettingBool
from lvfs.tests.models import Test
from lvfs.claims.models import Claim
from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentShard
from lvfs.util import _get_datestr_from_datetime


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "microcode-mcedb")
        self.name = "Microcode MCEdb"
        self.summary = "Check the microcode is not older than latest release"
        self.order_after = ["uefi-extract"]
        self.settings.append(
            PluginSettingBool(
                key="microcode_mcedb_enabled", name="Enabled", default=False
            )
        )
        self.settings.append(
            PluginSetting(
                key="microcode_mcedb_path",
                name="Path to MCE.db",
                default="MCExtractor/MCE.db",
            )
        )

    def require_test_for_md(self, md: Component) -> bool:
        # only run for capsule updates
        if not md.protocol:
            return False
        if not md.blob:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def _run_test_on_shard(self, test: Test, shard: ComponentShard) -> None:
        # only Intel μcode supported at this time
        if shard.guid != "3f0229ad-0a00-5269-90cf-0a45d8781b72":
            return

        # get required attributes
        cpuid = shard.get_attr_value("cpuid")
        if not cpuid:
            return
        platform = shard.get_attr_value("platform")
        if not platform:
            return
        version = shard.get_attr_value("version")
        if not version:
            return
        datestr = shard.get_attr_value("yyyymmdd")
        if not datestr:
            return

        # don't expect vendors to include microcode that was released *after*
        # the file was uploaded to the LVFS
        datestr_upload = str(_get_datestr_from_datetime(shard.md.fw.timestamp))

        # load database
        mcefn = self.get_setting("microcode_mcedb_path", required=True)
        if not os.path.exists(mcefn):
            test.add_fail(f"cannot locate database: {mcefn}")
            return
        conn = sqlite3.connect(mcefn)
        c = conn.cursor()
        c.execute(
            "SELECT version, yyyymmdd FROM Intel WHERE cpuid=? AND "
            "platform=? AND version>? AND yyyymmdd>? ORDER BY version LIMIT 1",
            (cpuid, platform, version, datestr_upload),
        )
        res = c.fetchone()
        if res:
            (
                newest_version,
                newset_datestr,
            ) = res
            print(
                f"CPUID:{int(cpuid, 16):#x} "
                f"Platform:{int(platform, 16):#x} "
                f"version {int(version, 16):#x} (released on {datestr}) may be older "
                f"than latest released version {int(newest_version, 16):#x} "
                f"(released on {newset_datestr})"
            )
            claim = (
                db.session.query(Claim).filter(Claim.kind == "old-microcode").first()
            )
            if claim:
                shard.md.add_claim(claim)
        c.close()

    def run_test_on_md(self, test: Test, md: Component) -> None:
        for shard in md.shards:
            self._run_test_on_shard(test, shard)

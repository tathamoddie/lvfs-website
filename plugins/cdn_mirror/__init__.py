#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import os
import hashlib
from typing import Optional
from io import BytesIO
import requests
from PIL import Image, UnidentifiedImageError
from flask import current_app as app

from cabarchive import CabArchive, NotSupportedError

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.components.models import Component
from lvfs.firmware.models import Firmware
from lvfs.tests.models import Test


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "cdn-mirror")
        self.name = "CDN Mirror"
        self.summary = "Mirror screenshots on the CDN for privacy"
        self.settings.append(
            PluginSettingBool(key="cdn_mirror_enable", name="Enabled", default=True)
        )

    def require_test_for_md(self, md: Component) -> bool:
        if not md.screenshot_url and not md.release_image:
            return False
        return True

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=False)
            fw.tests.append(test)

    def _cdn_mirror_file(self, test: Test, fw: Firmware, url: str) -> Optional[str]:
        # copy from archive or download
        if url.startswith("file://"):
            try:
                cabarchive = CabArchive(fw.blob, flattern=True)
            except (NotImplementedError, NotSupportedError) as e:
                test.add_fail("Failed to open archive", str(e))
                return None
            buf = cabarchive[url[7:]].buf
        else:
            try:
                r = requests.get(url, timeout=10)
                r.raise_for_status()
                buf = r.content
            except requests.exceptions.RequestException as e:
                test.add_fail("Download", str(e))
                return None

        # load as a PNG
        try:
            im = Image.open(BytesIO(buf))
        except UnidentifiedImageError as e:
            test.add_fail("Parse", str(e))
            return None
        if im.width > 800 or im.height > 600:
            test.add_fail("Size", f"{im.width}x{im.height} is too large")
        elif im.width < 300 or im.height < 100:
            test.add_warn("Size", f"{im.width}x{im.height} is too small")

        # save to download directory
        basename = f"img-{hashlib.sha256(buf).hexdigest()}.png"
        fn = os.path.join(app.config["DOWNLOAD_DIR"], basename)
        if not os.path.isfile(fn):
            im.save(fn, "PNG")

        # set the safe URL
        return os.path.join(app.config["CDN_DOMAIN"], "downloads", basename)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        if md.screenshot_url and not md.screenshot_url_safe:
            md.screenshot_url_safe = self._cdn_mirror_file(
                test, md.fw, md.screenshot_url
            )
        if md.release_image and not md.release_image_safe:
            md.release_image_safe = self._cdn_mirror_file(test, md.fw, md.release_image)


# run with PYTHONPATH=. ./env/bin/python3 plugins/cdn_mirror/__init__.py
if __name__ == "__main__":

    plugin = Plugin()
    _test = Test(plugin_id=plugin.id)
    _fw = Firmware()
    _md = Component()
    _md.screenshot_url = (
        "https://github.com/fwupd/8bitdo-firmware/raw/master/screenshots/FC30.png"
    )
    _fw.mds.append(_md)
    plugin.run_test_on_md(_test, _md)
    print("new URL", _md.screenshot_url_safe)
    for attribute in _test.attributes:
        print(attribute)

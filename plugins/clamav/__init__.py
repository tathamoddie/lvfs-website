#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import subprocess

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.tests.models import Test
from lvfs.firmware.models import Firmware


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self)
        self.name = "ClamAV"
        self.summary = "Check the firmware for trojans, viruses, malware and other malicious threats"
        self.settings.append(
            PluginSettingBool(key="clamav_enable", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingBool(
                key="clamav_detect_pua",
                name="Detect Possibly Unwanted Applications",
                default=False,
            )
        )
        self.settings.append(
            PluginSettingBool(
                key="clamav_use_daemon", name="Use clamd daemon", default=True
            )
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists on any component in the firmware
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, max_age=2592000)  # one month
            fw.tests.append(test)

    def run_test_on_fw(self, test, fw):
        # get ClamAV version
        if self.get_setting_bool("clamav_use_daemon"):
            argv = ["clamdscan", "--version"]
        else:
            argv = ["clamscan", "--version"]
        try:
            with subprocess.Popen(
                argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            ) as ps:
                if ps.wait() != 0:
                    if ps.stderr:
                        test.add_fail("Failed to scan", ps.stderr.read().decode())
                    return
                stdout, _ = ps.communicate()
        except OSError as e:
            test.add_fail("Failed to scan", e)
            return
        test.add_pass("Version", stdout.decode())

        # scan cabinet archive
        if self.get_setting_bool("clamav_use_daemon"):
            argv = [
                "clamdscan",
                "--fdpass",
                "--no-summary",
                fw.revisions[0].absolute_path,
            ]
        else:
            argv = [
                "clamscan",
                "--infected",
                "--scan-mail=no",
                "--phishing-scan-urls=no",
                "--phishing-sigs=no",
                "--scan-swf=no",
                "--nocerts",
                "--no-summary",
                fw.revisions[0].absolute_path,
            ]
            if self.get_setting_bool("clamav_detect_pua"):
                argv.append("--detect-pua=yes")
        try:
            with subprocess.Popen(
                argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            ) as ps:
                rc = ps.wait()
                if rc == 2:
                    if ps.stderr:
                        test.add_fail("Failed to scan", ps.stderr.read().decode())
                    return
                stdout, _ = ps.communicate()
        except OSError as e:
            test.add_fail("Failed to scan", str(e))
            return

        # parse results
        if rc != 0:
            for ln in stdout.decode().split("\n"):
                try:
                    _, status = ln.split(": ", 2)
                except ValueError:
                    continue
                test.add_fail("Results", status)

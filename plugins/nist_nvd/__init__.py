#!/usr/bin/python3
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access

import json
import datetime

import requests

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentIssue
from lvfs.pluginloader import PluginBase, PluginError, PluginSettingBool


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "nist_nvd")
        self.order_after = ["vince"]
        self.name = "NIST NVD"
        self.summary = "The U.S. government repository of vulnerability data"
        self.settings.append(PluginSettingBool(key="nist_nvd_enable", name="Enabled"))

    def _update_issue_json(self, issue: ComponentIssue, r_text: str) -> None:
        # parse JSON
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError(f"Failed to query: {r_text}") from e
        try:
            cve = data["result"]["CVE_Items"][0]["cve"]
            issue.description = cve["description"]["description_data"][0]["value"]
        except KeyError:
            pass
        try:
            published = data["result"]["CVE_Items"][0]["publishedDate"].split("Z")[0]
            issue.published = datetime.datetime.fromisoformat(published)
        except (KeyError, ValueError):
            pass

    def _update_issue(self, issue: ComponentIssue) -> None:
        # public search
        api = f"https://services.nvd.nist.gov/rest/json/cve/1.0/{issue.value}"
        print(f"requesting {api}…")
        try:
            r = requests.get(api, stream=True, timeout=10)
        except requests.exceptions.ReadTimeout as e:
            raise PluginError(f"Timeout from {api}") from e
        if r.status_code == 200:
            self._update_issue_json(issue, r.text)

    def archive_presign(self, fw: Firmware) -> None:
        # add description and published dates for CVEs
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind == "cve":
                    if not issue.description or not issue.published:
                        self._update_issue(issue)


# run with PYTHONPATH=. ./env/bin/python3 plugins/nist_nvd/__init__.py
if __name__ == "__main__":
    plugin = Plugin()
    _fw = Firmware()
    _md = Component()
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-0545"))
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-8696"))
    _fw.mds.append(_md)
    plugin.archive_presign(_fw)
    for _issue in _md.issues:
        print(f"issue {_issue.value} = {_issue.description} [{_issue.published}]")
